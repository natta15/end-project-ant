Project

- cd end-project-ant

Download

- Download Arduino : https://downloads.arduino.cc/arduino-1.8.19-windows.exe
- Download Java AdoptOpenJDK 11 : https://adoptopenjdk.net
- Download Blynk Server : https://github.com/blynkkk/blynk-server/releases/download/v0.41.17/server-0.41.17.jar

SetUp

- File > Preferences : Paste JSON into Additional Boards Manager URLs : 
  https://dl.espressif.com/dl/package_esp32_index.json, http://arduino.esp8266.com/stable/package_esp8266com_index.json
- Tools > Board > Boards Manage : search : esp32 or esp8266 for you select board hardware : install for board hardware for you 
- Sketch > Include Library > Manage Libraries : search : Blynk : install Blynk
- server.properties : https://github.com/blynkkk/blynk-server/blob/master/server/core/src/main/resources/server.properties

Run

- First Run : java -jar server-0.41.17.jar -dataFolder /end-project-ant/project/test
- And First Run : runStart.cmd
- Next Run : runProject.cmd
- Reset Run Project : runReset.cmd

Library 

- URL : https://github.com/blynkkk/blynk-library
- URL : https://github.com/adafruit/Adafruit-PWM-Servo-Driver-Library