
#define BLYNK_PRINT Serial

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

char auth[] = "";

char ssid[] = "";
char pass[] = "";

void setup()
{
  Serial.begin(115200);
  Blynk.begin(auth, ssid, pass,"192.168.1.111",9443);
}

void loop()
{
  Blynk.run();
}
