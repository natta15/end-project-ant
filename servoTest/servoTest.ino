COPY
#include <Servo.h>

Servo myservo;  

int potpin = 0;  // A0 for UNO 
int val;    

void setup() {
  myservo.attach(9);  
}

void loop() {
  val = analogRead(potpin);           
  val = map(val, 0, 1023, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
  myservo.write(val);                  
  delay(15);                           
}
