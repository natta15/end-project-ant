#include <Wire.h>

#include <Adafruit_PWMServoDriver.h>
 
Adafruit_PWMServoDriver pca9685 = Adafruit_PWMServoDriver(0x40);
 
#define SERVOMIN  80  
#define SERVOMAX  600  
#define SER0  0   
#define SER1  12  
 
int pwm0;
int pwm1;
 
void setup() {

  Serial.begin(115200);
  Serial.println("PCA9685 Servo Test");
  pca9685.begin();
  pca9685.setPWMFreq(50);
 
}
 
void loop() {
 
 
  for (int posDegrees = 0; posDegrees <= 180; posDegrees++) {
    pwm0 = map(posDegrees, 0, 180, SERVOMIN, SERVOMAX);
    pca9685.setPWM(SER0, 0, pwm0);
    Serial.print("Motor 0 = ");
    Serial.println(posDegrees);
    delay(30);
  }
  for (int posDegrees = 180; posDegrees >= 0; posDegrees--) {
    pwm1 = map(posDegrees, 0, 180, SERVOMIN, SERVOMAX);
    pca9685.setPWM(SER1, 0, pwm1);
    Serial.print("Motor 1 = ");
    Serial.println(posDegrees);
    delay(30);
  }
  for (int posDegrees = 180; posDegrees >= 0; posDegrees--) {
    pwm0 = map(posDegrees, 0, 180, SERVOMIN, SERVOMAX);
    pca9685.setPWM(SER0, 0, pwm0);
    Serial.print("Motor 0 = ");
    Serial.println(posDegrees);
    delay(30);
  }
 
  for (int posDegrees = 0; posDegrees <= 180; posDegrees++) {
    pwm1 = map(posDegrees, 0, 180, SERVOMIN, SERVOMAX);
    pca9685.setPWM(SER1, 0, pwm1);
    Serial.print("Motor 1 = ");
    Serial.println(posDegrees);
    delay(30);
  }
 
 
}
